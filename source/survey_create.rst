********************
Gestión de encuestas
********************

Audios
******

Cada encuesta confeccionada implica 3 clases audios a reproducir:

* Saludo inicial
* Despedida
* Preguntas 

Antes de crear nuestras encuestas, debemos asegurarnos de subir previamente los audios involucrados en la misma utilizando el módulo de audios de OMniLeads:
*Telefonía -Audios - Audios personalizados*.

Una vez disponibles los audios con los que vamos a confeccionar nuestra encuesta, entonces avanzamos !

Nueva encuesta
**************

El Addon i-Survey se debe visualizar dentro del Menú principal: 

.. image:: images/survey_module.png

Desde allí podemos generar una nueva encuesta:

.. image:: images/survey_create_01.png

La primera vista de creación de encuestas implica los siguientes campos:

* **Nombre**: El nombre que va a llevar la nueva encuesta.
* **Timeout**: El tiempo de tolerancia en el que se espera una respuesta DTMF como interacción.
* **Time retry**: El tiempo en que va a esperar para volver a reproducir cada pregunta.
* **Retry**: La cantidad de veces que va a tolerar una respuesta no listada como opción.
* **Start audio**: El audio que se dispara como bienvenida en cada llamada transferida a la encuesta.
* **End audio**: El audio que se dispara al momento de finalizar la encuesta.

Una vez completado el formulario la encuesta pasa a estar disponible para seguir su confección, asignación y posterior publicación.

.. image:: images/survey_create_02.png

Confección de preguntas
***********************

En el orden correcto primero debemos generar todas las preguntas que va a confeccionar nuestra encuesta, para luego comenzar a asociar las posibles respuestas 
sobre cada pregunta y sus acciones a disparar. Ahora nos vamos a centrar en las preguntas, para ello se hace click en el ícono que lleva un '?' dentro de 
la encuesta en cuestión.

Para ejemplificar se considera una encuesta de satisfacción post-atención de dos preguntas:

Como se puede observar en la confección de la pregunta solamente se debe indicar el audio  a reproducir, un nombre a la pregunta, una descripción y luego indicar 
si se trata de la primera pregunta de la encuesta por un lado y si la pregunta es ponderable, es decir que se debe promediar la respuesta en los reportes.

* **Cómo calificaría el nivel de atención recibido del 1 al 5**

.. image:: images/survey_create_03.png

* **El motivo de su llamado fue resuelto**

.. image:: images/survey_create_04.png

Confección de las respuestas de cada pregunta 
**********************************************

Una vez generadas todas las preguntas, se deben crear las respuestas esperadas por cada pregunta junto a la acción que debe disparar cada una de éstas.
Siguiendo el ejemplo planteado, se cuenta con dos preguntas:

.. image:: images/survey_create_05.png

A continuación se trabaja con la confección de respuestas para la pregunta inicial:

.. image:: images/survey_create_07.png

Como se puede observar se trata de asociar cada DTMF esperado como respuesta con una acción: 

* Finalizar 
* Repetir
* Próxima pregunta 

Es decir que el hecho de recibir como interacción el DTMF esperado por un lado *califica* la interacción y por el otro dispara una de las acciones citadas.
Como podemos observar, cada una de las respuestas llevan a reproducir la próxima pregunta, osea la pregunta número dos de la encuesta. 

.. image:: images/survey_create_06.png

.. note::

    Esto no necesariamente debe ser así, en un ejemplo más complejo se podrían anidar muchas preguntas en diferentes niveles formando un árbol de la complejidad 
    que se desee.

La segunda y última pregunta de nuestro ejemplo:

.. image:: images/survey_create_09.png

Cada una de las respuestas a ésta pregunta, tiene como acción terminar con la encuesta.

Asignación a campaña y publicación de la encuesta a producción:
***************************************************************

Se debe asociar la encuesta con una campaña. 

.. image:: images/survey_create_08.png


.. note::

    Solo se puede asociar una encuesta a una campaña. No es viable asociar una encuesta a más de una campaña.

Finalmente para dejar en producción la encuesta se la debe publicar.

.. image:: images/survey_create_11.png

.. image:: images/survey_create_10.png

Transferencia de la llamada a la encuesta
*****************************************

Para encuesta post-atención el agente deberá transferir la llamada a la encuesta pertinente a la campaña. Para ello debe hundir el botón de *transferir*
a partir del cual se va a desplegar una ventana modal con la opción para transferir a la encuesta.

.. image:: images/survey_create_12.png