��          T               �   !   �   j  �        
   ,  �   7     �  �  �  !   }  C  �     �     �  �        �   **release-1.21.0** para adelante. Dentro del ecosistema de extensiones y addons que pueden agregarse a OmniLeads, i-Survey nos permite someter una llamada (entrante o saliente) a una encuesta telefónica. A través de una simple interfaz el usuario podrá diseñar desde encuestas de satisfacción bien sencillas hasta de las más complejas campañas de sondeo con árboles de preguntas anidadas. Que es i-Survey ? Requisitos Todas las encuestas podrán ser analizadas mediante el módulo de reportes, el cual nos permite desglozar detalladamente el feedback recolectado mediante las campañas. Una instancia de OMniLeads Project-Id-Version: survey app docs 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2022-04-04 10:39-0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: en
Language-Team: en <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.1
 **release-1.21.0** para adelante. Within the ecosystem of extensions and addons that can be added to OmniLeads, i-Survey allows us to guide a call (incoming or outgoing)  to a telephone survey. Through a simple interface, the user will be able to design from very simple satisfaction surveys to the more complex polling campaigns with nested question trees. What is i-Survey ? Requirements All surveys can be analyzed through the report module,  which allows us to break down in detail the feedback collected through the campaigns. OMniLeads instance 