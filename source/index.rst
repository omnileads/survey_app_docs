.. Omnileads Installation documentation master file, created by
   sphinx-quickstart on Wed Apr 30 10:52:05 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

************************
OMniLeads i-Survey Addon
************************

Que es i-Survey ?
*****************

Dentro del ecosistema de extensiones y addons que pueden agregarse a OmniLeads, i-Survey nos permite someter una llamada (entrante o saliente) a una encuesta telefónica. 
A través de una simple interfaz el usuario podrá diseñar desde encuestas de satisfacción bien sencillas hasta de las más complejas campañas de sondeo con árboles 
de preguntas anidadas. 

Todas las encuestas podrán ser analizadas mediante el módulo de reportes, el cual nos permite desglozar detalladamente el feedback recolectado mediante 
las campañas. 

Requisitos
***********

1. Una instancia de OMniLeads

- **release-1.21.0** para adelante.

.. toctree::
   :hidden:

   install.rst
   survey_create.rst
   survey_reports.rst
   changelog.rst
