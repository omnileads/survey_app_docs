*********************
Reportes de Encuestas
*********************

Filtros de Reportes
*******************

Al clickear sobre el ícono de reportes, se despliega el Filtro Avanzado del módulo de Encuestas, donde se pueden seleccionar:

* Rango de Fechas: Período de tiempo para la evaluación de resultados de la encuesta de calidad.
* Campañas: la campaña involucrada en la encuesta.
* Agentes: los agentes que se auditan a partir de la encuesta.
* Grupos: los grupos de agentes que se auditan a partir de la encuesta.

.. image:: images/survey_rep_general.png

Al presionar sobre el botón "Buscar", se despliegan los múltiples resultados relacionados a la encuesta disparada. Por otro lado
el botón "Descargar CSV" permite obtener un archivo en formato CSV/Excel a partir del cual se obtiene una sábana de registros con sus archivos
de audio y las opciones seleccionadas en cada una de las secciones de la encuesta.

Resultados de Reporte
*********************

A la hora de evaluar los reportes de encuestas, partimos de una serie de Tabs que organizan la información en diferentes secciones:

* Reporte General: ofrece cantidades de llamadas derivadas a encuesta, completadas, parcialmente completadas y abandonadas durante la reproducción de los audios asociados.

.. image:: images/survey_rep_generalreportes.png


* Calificaciones por Pregunta: para cada pregunta de la encuesta confeccionada, se grafican las diferentes calificaciones efectuadas por el llamante a la hora de responder la encuesta.

.. image:: images/survey_rep_byQ.png


* Respuestas por Agente: para cada pregunta de la encuesta confeccionada, se grafican las diferentes opciones seleccionadas por el llamante, por agente.

.. image:: images/survey_rep_byAg.png


* Respuestas por Fecha: para cada pregunta de la encuesta confeccionada, se grafican las diferentes opciones seleccionadas por el llamante, por fecha.

.. image:: images/survey_rep_byDates.png


* Promedios por Agente: para cada pregunta "ponderada", se obtiene el promedio de las opciones seleccionadas por el llamante, por agente.

.. image:: images/survey_rep_byAvgAg.png


* Promedios por Fecha: para cada pregunta "ponderada", se obtiene el promedio de las opciones seleccionadas por el llamante, por dia.

.. image:: images/survey_rep_byAvgDate.png

Es importante aclarar que cada una de las secciones de reportes presentadas en las diferentes vistas, es exportable a CSV/EXcel para su correcto análisis.

.. image:: images/survey_rep_export.png

.. image:: images/survey_rep_export2.png

.. image:: images/survey_rep_export3.png

.. image:: images/survey_rep_export4.png

















