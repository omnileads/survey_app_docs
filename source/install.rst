******************
Cómo lo obtengo ?
******************

Instalación
************

Para instalar este Addon, debe:

1. Descargar el .tar.gz que se encuentra disponible en `esta URL <https://fts-public-packages.s3-sa-east-1.amazonaws.com/survey_app/survey_app-latest.tar.gz>`_ **dentro de una instancia con OMniLeads instalado**, en la siguiente ubicación: `/opt/omnileads/addons`
2. Ejecutar los siguientes comandos con el usuario root:

.. code::

  # cd /opt/omnileads/addons 
  # wget https://fts-public-packages.s3.sa-east-1.amazonaws.com/survey_app/survey_app-1.0.2.tar.gz
  # tar xzvf premium_reports_app-latest.tar.gz
  # cd premium_reports_app
  # ./install.sh

1. El mismo instalador le dirá si terminó satisfactoriamente.
2. Ejecutar `service omnileads restart`, tal como dice el instalador.


Registro de Key OMniLeads
**************************

Es condición necesaria que la instancia esté registrada, acción que se efectúa desde el Menú
Ayuda>Registrar del planel de Administración:

.. image:: images/registro-oml.png
        :align: center

i-Survey está disponible para versiones de OmniLeads 1.24.0 o superiores. Si tiene dudas en cómo obtener la key de activación, contacte a info@omnileads.net.

Acerca del Licenciamiento
**************************

i-Survey es un módulo comercial, motivo por el cual su funcionamiento está sujeto a la activación de producto a partir de la vinculación de una key (llave de acceso) con el Servidor de Llaves.

El esquema de licenciamiento es de carácter permanente (key definitiva), y se brinda 1 año de actualizaciones para cobertura de bug-fixing y nuevas features sobre el release instalado. En caso de precisar upgrades de releases superado el año de la compra, se puede actualizar la key a un costo módico.

La key permite activar el módulo en 1 (una) sola instancia a la vez, y no está sujeta a la cantidad de agentes operando en la plataforma.

Si tiene dudas en cómo obtener la key de activación, contacte a info@omnileads.net.
